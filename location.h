#ifndef LOCATION_H
#define LOCATION_H
#define RESOLUTION 0.15

#include<sys/types.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<vector>
#include "ckobuki.h"
#include "rplidar.h"



typedef struct
{
    double x;
    double y;
    signed short IncrementLeft;
    signed short IncrementRight;
    unsigned short oldEncoderLeft;
    unsigned short oldEncoderRight;
    double totalLeftLength;
    double totalRightLength;
    double totalLength;
    double oldFi;
    double angle;
    double deltaAlpha;
    bool firstAngle;
}LocationData;

typedef struct
{
    bool enabled;
    double x;
    double y;
    double speed;
    double minDistance;
    double deadAngle;
    double errorAngle;
    double p;
    double p_angle;
    double max_speed;
}NavigationData;


typedef struct{
    bool enabled;
    bool toTxt;
    double resolution = RESOLUTION;
    bool isRotating;
    int map[80][80];
}MapData;

class Node{

    int x;
    int y;
    int value;
public:
    Node(int dx, int dy, int dv)
    {
        x = dx;
        y = dy;
        value = dv;
    }

    int getX()
       {
           return x;
       }
   int getY()
       {
           return y;
       }
   int getValue()
       {
           return value;
       }
   void setValue(int dv)
       {
            value = dv;
       }

};



typedef struct{
    bool enabled;
    int finishX;
    int finishY;
    vector<Node*> path;
    vector<Node*> filteredPath;
}FloodFillData;


class Corner{

    double x;
    double y;
    double angle;
    bool outerLeft;
public:
    Corner(double dx, double dy, double an, bool left)
    {
        x = dx;
        y = dy;
        angle = an;
        outerLeft = left;
    }

   double getX(){ return x;}
   double getY(){ return y;}
   double getAngle(){ return angle;}
   bool isOuterLeft(){ return outerLeft;}
};

typedef struct{
    bool enabled;
    bool cornerFound;
    bool finishSeen;
    double finishX;
    double finishY;
    double zoneDiameter;
    double lengthA;
    double lengthB;
    double nextX;
    double nextY;
    vector<Corner*> corners;
}BugData;

#endif // LOCATION_H
