#define _USE_MATH_DEFINES
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPainter>
#include <math.h>
#include <location.h>
#include <list>
#include <algorithm>
#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif
#include <math.h>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    //tu je napevno nastavena ip. treba zmenit na to co ste si zadali do text boxu alebo nejaku inu pevnu. co bude spravna
    ipaddress="127.0.0.1";
    //cap.open("http://192.168.1.11:8000/stream.mjpg");
    ui->setupUi(this);
    datacounter=0;
    //  timer = new QTimer(this);
    //   connect(timer,  SIGNAL(timeout()),  this,  SLOT(getNewFrame()));
    actIndex=-1;
    useCamera=false;


    locationData.x = 0;
    locationData.y = 0;
    locationData.firstAngle = false;
    datacounter=0;



}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.setBrush(Qt::black);
    QPen pero;
    pero.setStyle(Qt::SolidLine);
    pero.setWidth(3);
    pero.setColor(Qt::green);
    QRect rect(20, 120, 700, 500);
    rect= ui->frame->geometry();
    rect.translate(0, 15);
    painter.drawRect(rect);

    /*  if(useCamera==true)
    {
        std::cout<<actIndex<<std::endl;
        QImage image = QImage((uchar*)frame[actIndex].data,  frame[actIndex].cols,  frame[actIndex].rows,  frame[actIndex].step,  QImage::Format_RGB888  );
        painter.drawImage(rect, image.rgbSwapped());
    }
    else*/
    {
        if(updateLaserPicture==1)
        {
            updateLaserPicture=0;

            painter.setPen(pero);
            //teraz tu kreslime random udaje... vykreslite to co treba... t.j. data z lidaru
            //std::cout<<copyOfLaserData.numberOfScans<<std::endl;
            for(int k=0;k<copyOfLaserData.numberOfScans/*360*/;k++)
            {
                /*  int dist=rand()%500;
            int xp=rect.width()-(rect.width()/2+dist*2*sin((360.0-k)*3.14159/180.0))+rect.topLeft().x();
            int yp=rect.height()-(rect.height()/2+dist*2*cos((360.0-k)*3.14159/180.0))+rect.topLeft().y();*/
                int dist=copyOfLaserData.Data[k].scanDistance/20;
                int xp=rect.width()-(rect.width()/2+dist*2*sin((360.0-copyOfLaserData.Data[k].scanAngle)*3.14159/180.0))+rect.topLeft().x();
                int yp=rect.height()-(rect.height()/2+dist*2*cos((360.0-copyOfLaserData.Data[k].scanAngle)*3.14159/180.0))+rect.topLeft().y();
                if(rect.contains(xp, yp))
                    painter.drawEllipse(QPoint(xp,  yp), 2, 2);
            }
        }
    }
}

void MainWindow::setUiValues(double robotX,  double robotY,  double robotFi, double errorAngle)
{
    ui->lineEdit_2->setText(QString::number(robotX));
    ui->lineEdit_3->setText(QString::number(robotY));
    ui->lineEdit_4->setText(QString::number(robotFi));
    ui->lineEdit_7->setText(QString::number(errorAngle));
}

double degreeToRad(double degree){
    return degree * (M_PI/180.0);
}

double radToDegree(double rad){
    return rad * (180/M_PI);
}

void MainWindow::processThisRobot()
{

    //float gyroAngle = robotdata.GyroAngle/100.0 *M_PI/180;


    locationData.IncrementLeft  = (short int)(robotdata.EncoderLeft + 32768 -  locationData.oldEncoderLeft) % 65536 - 32768;
    locationData.IncrementRight = (short int)(robotdata.EncoderRight + 32768 -  locationData.oldEncoderRight) % 65536 - 32768;

    locationData.totalLeftLength = getWheelLength(robot.getTickToMeter(), locationData.IncrementLeft);
    locationData.totalRightLength = getWheelLength(robot.getTickToMeter(), locationData.IncrementRight);
    locationData.totalLength = (locationData.totalLeftLength + locationData.totalRightLength)/2.0;
    locationData.deltaAlpha = ((locationData.totalRightLength - locationData.totalLeftLength)/robot.getDiameter());
    locationData.angle = locationData.oldFi + locationData.deltaAlpha;
    locationData.angle = locationData.angle-2.0*M_PI*floor(locationData.angle/(2.0*M_PI)+0.5);

    locationData.x = getNewLocation(locationData.angle, locationData.x, locationData.totalLength, 1);
    locationData.y = getNewLocation(locationData.angle, locationData.y, locationData.totalLength, 0);

    emit uiValuesChanged(locationData.x, locationData.y,radToDegree(locationData.angle),  radToDegree(navigationData.errorAngle));
    navigateTo();
    navigateThroughGrid();
    bugAlgorithm();

    locationData.oldEncoderLeft = robotdata.EncoderLeft;
    locationData.oldEncoderRight = robotdata.EncoderRight;
    locationData.oldFi = locationData.angle;
}

void MainWindow::processThisLidar(LaserMeasurement &laserData)
{
    memcpy( &copyOfLaserData, &laserData, sizeof(LaserMeasurement));
    //tu mozete robit s datami z lidaru.. napriklad najst prekazky,  zapisat do mapy. naplanovat ako sa prekazke vyhnut.
    // ale nic vypoctovo narocne - to iste vlakno ktore cita data z lidaru
    updateLaserPicture=1;
    update();//tento prikaz prinuti prekreslit obrazovku.. zavola sa paintEvent funkcia
    createMap();
    findCorners();

}

void MainWindow::on_pushButton_9_clicked() //start button
{
    mapData.isRotating = false;
    //tu sa nastartuju vlakna ktore citaju data z lidaru a robota
    laserthreadHandle=CreateThread(NULL, 0, laserUDPVlakno,  (void *)this, 0, &laserthreadID);
    robotthreadHandle=CreateThread(NULL, 0, robotUDPVlakno,  (void *)this, 0, &robotthreadID);


    /*  laserthreadID=pthread_create(&laserthreadHandle, NULL, &laserUDPVlakno, (void *)this);
      robotthreadID=pthread_create(&robotthreadHandle, NULL, &robotUDPVlakno, (void *)this);*/
    connect(this, SIGNAL(uiValuesChanged(double, double, double, double)), this, SLOT(setUiValues(double, double, double, double)));

}

void MainWindow::on_pushButton_2_clicked() //forward
{
    //pohyb dopredu
    std::vector<unsigned char> mess=robot.setTranslationSpeed(400);
    if (sendto(rob_s,  (char*)mess.data(),  sizeof(char)*mess.size(),  0,  (struct sockaddr*) &rob_si_posli,  rob_slen) == -1)
    {

    }
    mapData.isRotating = false;
}

void MainWindow::on_pushButton_3_clicked() //back
{
    std::vector<unsigned char> mess=robot.setTranslationSpeed(-400);
    if (sendto(rob_s,  (char*)mess.data(),  sizeof(char)*mess.size(),  0,  (struct sockaddr*) &rob_si_posli,  rob_slen) == -1)
    {

    }
    mapData.isRotating = false;
}

void MainWindow::on_pushButton_6_clicked() //left
{
    mapData.isRotating = true;
    std::vector<unsigned char> mess=robot.setRotationSpeed(3.14159/4);
    if (sendto(rob_s,  (char*)mess.data(),  sizeof(char)*mess.size(),  0,  (struct sockaddr*) &rob_si_posli,  rob_slen) == -1)
    {

    }
}

void MainWindow::on_pushButton_5_clicked()//right
{
    mapData.isRotating = true;
    std::vector<unsigned char> mess=robot.setRotationSpeed(-3.14159/4);
    if (sendto(rob_s,  (char*)mess.data(),  sizeof(char)*mess.size(),  0,  (struct sockaddr*) &rob_si_posli,  rob_slen) == -1)
    {

    }
}

void MainWindow::on_pushButton_4_clicked() //stop
{
    navigationData.enabled = false;
    mapData.isRotating = false;
    std::vector<unsigned char> mess=robot.setTranslationSpeed(0);
    if (sendto(rob_s,  (char*)mess.data(),  sizeof(char)*mess.size(),  0,  (struct sockaddr*) &rob_si_posli,  rob_slen) == -1)
    {

    }

}

void MainWindow::laserprocess()
{
    WSADATA wsaData = {0};
    int iResult = 0;



    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2,  2),  &wsaData);
    las_slen = sizeof(las_si_other);
    if ((las_s=socket(AF_INET,  SOCK_DGRAM,  IPPROTO_UDP)) == -1)
    {

    }

    int las_broadcastene=1;
    setsockopt(las_s, SOL_SOCKET, SO_BROADCAST, (char*)&las_broadcastene, sizeof(las_broadcastene));
    // zero out the structure
    memset((char *) &las_si_me,  0,  sizeof(las_si_me));

    las_si_me.sin_family = AF_INET;
    las_si_me.sin_port = htons(52999);
    las_si_me.sin_addr.s_addr = htonl(INADDR_ANY);

    las_si_posli.sin_family = AF_INET;
    las_si_posli.sin_port = htons(5299);
    las_si_posli.sin_addr.s_addr = inet_addr(ipaddress.data());//htonl(INADDR_BROADCAST);
    bind(las_s ,  (struct sockaddr*)&las_si_me,  sizeof(las_si_me) );
    char command=0x00;
    if (sendto(las_s,  &command,  sizeof(command),  0,  (struct sockaddr*) &las_si_posli,  las_slen) == -1)
    {

    }
    LaserMeasurement measure;
    while(1)
    {
        if ((las_recv_len = recvfrom(las_s,  (char*)&measure.Data,  sizeof(LaserData)*1000,  0,  (struct sockaddr *) &las_si_other,  (int*)&las_slen)) == -1)
        {

            continue;
        }
        measure.numberOfScans=las_recv_len/sizeof(LaserData);
        //tu mame data..zavolame si funkciu

        //     memcpy(&sens, buff, sizeof(sens));


        processThisLidar(measure);




    }
}


void MainWindow::robotprocess()
{
    WSADATA wsaData = {0};
    int iResult = 0;



    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2,  2),  &wsaData);

    if ((rob_s=socket(AF_INET,  SOCK_DGRAM,  IPPROTO_UDP)) == -1)
    {

    }

    char rob_broadcastene=1;
    setsockopt(rob_s, SOL_SOCKET, SO_BROADCAST, &rob_broadcastene, sizeof(rob_broadcastene));
    // zero out the structure
    memset((char *) &rob_si_me,  0,  sizeof(rob_si_me));

    rob_si_me.sin_family = AF_INET;
    rob_si_me.sin_port = htons(53000);
    rob_si_me.sin_addr.s_addr = htonl(INADDR_ANY);

    rob_si_posli.sin_family = AF_INET;
    rob_si_posli.sin_port = htons(5300);
    rob_si_posli.sin_addr.s_addr =inet_addr(ipaddress.data());//inet_addr("10.0.0.1");// htonl(INADDR_BROADCAST);
    rob_slen = sizeof(rob_si_me);
    bind(rob_s ,  (struct sockaddr*)&rob_si_me,  sizeof(rob_si_me) );

    std::vector<unsigned char> mess=robot.setDefaultPID();
    if (sendto(rob_s,  (char*)mess.data(),  sizeof(char)*mess.size(),  0,  (struct sockaddr*) &rob_si_posli,  rob_slen) == -1)
    {

    }
    Sleep(100);
    mess=robot.setSound(440, 1000);
    if (sendto(rob_s,  (char*)mess.data(),  sizeof(char)*mess.size(),  0,  (struct sockaddr*) &rob_si_posli,  rob_slen) == -1)
    {

    }
    unsigned char buff[50000];
    while(1)
    {
        memset(buff, 0, 50000*sizeof(char));
        if ((rob_recv_len = recvfrom(rob_s,  (char*)&buff,  sizeof(char)*50000,  0,  (struct sockaddr *) &rob_si_other, (int*) &rob_slen)) == -1)
        {

            continue;
        }
        //tu mame data..zavolame si funkciu

        //     memcpy(&sens, buff, sizeof(sens));
        //struct timespec t;
        //      clock_gettime(CLOCK_REALTIME, &t);

        int returnval=robot.fillData(robotdata, (unsigned char*)buff);


        if(returnval==0)
        {
            if(!locationData.firstAngle){
                locationData.oldEncoderLeft = robotdata.EncoderLeft;
                locationData.oldEncoderRight =  robotdata.EncoderRight;
                locationData.firstAngle = true;
            }
            processThisRobot();
        }


    }
}

void MainWindow::on_pushButton_clicked()
{
    if(useCamera==true)
    {
        useCamera=false;
        timer->stop();
        ui->pushButton->setText("use camera");
    }
    else
    {
        useCamera=true;
        timer->start(30);
        ui->pushButton->setText("use laser");
    }
}

void MainWindow::getNewFrame()
{

}

double MainWindow::getWheelLength(float tickToMeter,  signed short encoder){
    return tickToMeter * (encoder ) * 1.0;
}

double MainWindow::getNewLocation(float angle, float oldLocation, float length, int xbool){
    if(xbool == 1){
        return oldLocation + length * cos(angle);
    }else{
        return oldLocation + length * sin(angle);
    }
}

void MainWindow::on_pushButton_10_clicked()
{
    navigationData.enabled = true;
    navigationData.minDistance = 0.05;
    navigationData.deadAngle = degreeToRad(2);
    navigationData.x = ui->lineEdit_5->text().toDouble();
    navigationData.y = ui->lineEdit_6->text().toDouble();
    navigationData.p = 450;
    navigationData.p_angle = 2;
    navigationData.max_speed = 400;
}

void MainWindow::navigateTo(){
    if(navigationData.enabled && !floodFillData.enabled ){
        double distance = sqrt(pow(locationData.x - navigationData.x, 2) + pow(locationData.y - navigationData.y, 2));

        if(distance < navigationData.minDistance){
            navigationData.enabled = false;
            printf("FINISHED\n");
            std::vector<unsigned char> mess=robot.setTranslationSpeed(0);
            if (sendto(rob_s,  (char*)mess.data(),  sizeof(char)*mess.size(),  0,  (struct sockaddr*) &rob_si_posli,  rob_slen) == -1)
            {

            }
        }else{
            navigationData.errorAngle = locationData.angle - atan2((navigationData.y-locationData.y),(navigationData.x-locationData.x));
            navigationData.errorAngle = navigationData.errorAngle-2.0*M_PI*floor(navigationData.errorAngle/(2.0*M_PI)+0.5);

            if(-navigationData.deadAngle <= navigationData.errorAngle && navigationData.deadAngle >= navigationData.errorAngle ){

                float speed = 0;
                //pohyb
                if(distance*navigationData.p > navigationData.max_speed){
                    speed = navigationData.max_speed;
                }else{
                    speed = distance*navigationData.p;
                }

                std::vector<unsigned char> mess=robot.setTranslationSpeed(speed);
                if (sendto(rob_s,  (char*)mess.data(),  sizeof(char)*mess.size(),  0,  (struct sockaddr*) &rob_si_posli,  rob_slen) == -1) { }
                mapData.isRotating = false;
            }else{
                mapData.isRotating = true;
                if(-navigationData.deadAngle <= navigationData.errorAngle){
                    std::vector<unsigned char> mess = robot.setRotationSpeed(-abs(navigationData.errorAngle*navigationData.p_angle));
                    if (sendto(rob_s,  (char*)mess.data(),  sizeof(char)*mess.size(),  0,  (struct sockaddr*) &rob_si_posli,  rob_slen) == -1){}
                }else{
                    std::vector<unsigned char> mess = robot.setRotationSpeed(abs(navigationData.errorAngle*navigationData.p_angle));
                    if (sendto(rob_s,  (char*)mess.data(),  sizeof(char)*mess.size(),  0,  (struct sockaddr*) &rob_si_posli,  rob_slen) == -1){}
                }

            }

        }
    }
}


void MainWindow::createMap(){
    if(mapData.enabled && !mapData.isRotating ){

        for(int k=0;k<copyOfLaserData.numberOfScans/*360*/;k++)
        {

            mapData.resolution = 0.15;
            double dist=copyOfLaserData.Data[k].scanDistance/1000;//mm to m

            if(dist>0){
                double xp=locationData.x+dist*cos(locationData.angle+((360.0-copyOfLaserData.Data[k].scanAngle)*3.14159/180.0));
                double yp=locationData.y+dist*sin(locationData.angle+((360.0-copyOfLaserData.Data[k].scanAngle)*3.14159/180.0));

                int x =  xp/mapData.resolution + 39;
                int y =  39 - yp/mapData.resolution;

                mapData.map[y][x]= 1;

            }
        }
    }
}


void MainWindow::prepareMap(int radius){

    for(int rows = 0; rows < 80; rows++)
    {
        for(int columns = 0; columns < 80; columns++)
        {
            if(mapData.map[rows][columns] == 1 &&(rows+radius)<80 && (columns+radius)<80 && (rows-radius)>0 && (columns-radius)>0){

                for(int i =1;i<=radius; i++){
                    mapData.map[rows+i][columns] = (mapData.map[rows+i][columns] == 0) ? 2 : mapData.map[rows+i][columns];
                    mapData.map[rows-i][columns] = (mapData.map[rows-i][columns] == 0) ? 2 : mapData.map[rows-i][columns];
                    mapData.map[rows][columns+i] = (mapData.map[rows][columns+i] == 0) ? 2 : mapData.map[rows][columns+i];
                    mapData.map[rows][columns-i] = (mapData.map[rows][columns-i] == 0) ? 2 : mapData.map[rows][columns-i];
                    mapData.map[rows+i][columns+i] = (mapData.map[rows+i][columns+i] == 0) ? 2 : mapData.map[rows+i][columns+i];
                    mapData.map[rows-i][columns-i] = (mapData.map[rows-i][columns-i] == 0) ? 2 : mapData.map[rows-i][columns-i];
                    mapData.map[rows+i][columns-i] = (mapData.map[rows+i][columns-i] == 0) ? 2 : mapData.map[rows+i][columns-i];
                    mapData.map[rows-i][columns+i] = (mapData.map[rows-i][columns+i] == 0) ? 2 : mapData.map[rows-i][columns+i];
                }
            }

        }
    }

    for(int rows = 0; rows < 80; rows++)
    {
        for(int columns = 0; columns < 80; columns++)
        {
            if(mapData.map[rows][columns] == 2)
                mapData.map[rows][columns]=1;

            if(mapData.map[rows][columns] == 1){
                printf("x");
            }else{
                printf(" ");
            }
        }
        printf("\n");
    }


}

//nacitanie a zvacsenie hran
void MainWindow::on_pushButton_11_clicked()
{
    ifstream file("map.txt");
    if(file.fail()){
        printf("We can't find map.txt...");
    }else{
        printf("Loading map...");

        for(int rows = 0; rows < 80; rows++)
        {
            for(int columns = 0; columns < 80; columns++)
            {
                file >> mapData.map[rows][columns];
                if(mapData.map[rows][columns] == 1){
                    printf("x");
                }else{
                    printf(" ");
                }
            }
            printf("\n");
        }
        file.close();

        prepareMap(1);


    }
}


void MainWindow::on_radioButton_clicked()
{
    mapData.enabled = true;
    printf("Mapping enabled/n");

    for(int rows = 0; rows < 80; rows++){
        for(int columns = 0; columns < 80; columns++){
            mapData.map[rows][columns] = 0;
        }
    }
}


void MainWindow::on_radioButton_2_clicked()
{
    mapData.enabled = false;
    printf("Mapping disabled\n");
    printf("Created Map:\n");
    ofstream file("map.txt");
    for(int rows = 0; rows < 80; rows++)
    {
        for(int columns = 0; columns < 80; columns++)
        {
            if(mapData.map[rows][columns] == 1){
                printf("x");
            }else{
                printf(" ");
            }
            file << mapData.map[rows][columns]<<"";
        }
        printf("\n");
        file << endl;
    }
    file.close();
    printf("Saving...");
}

//samotny flood fill s retrace pathom
void MainWindow::on_pushButton_12_clicked()
{
    floodFillData.enabled = false;
    floodFillData.finishX = ui->lineEdit_8->text().toInt();
    floodFillData.finishY = ui->lineEdit_9->text().toInt();
    //premazanie mapy
    for(int rows = 0; rows < 80; rows++)
    {
        for(int columns = 0; columns < 80; columns++)
        {
            if(mapData.map[rows][columns] != 1 && mapData.map[rows][columns] != 0 ){
                mapData.map[rows][columns] =0;
            }

        }
        printf("\n");
    }

    //nastavenie zaciatku
    mapData.map[floodFillData.finishY][floodFillData.finishX] = 2;
    mapData.resolution = 0.15;
    int x =  locationData.x/mapData.resolution + 39;
    int y =  39 - locationData.y/mapData.resolution;

    vector<Node*> openLayerList, closeLayerList;
    Node *startNode =  new Node(floodFillData.finishX,floodFillData.finishY ,mapData.map[floodFillData.finishY][ floodFillData.finishX]);
    Node *finishNode =  new Node(x,y,0);

    openLayerList.push_back(new Node(floodFillData.finishX,floodFillData.finishY ,mapData.map[floodFillData.finishY][ floodFillData.finishX]));
    closeLayerList.push_back( new Node(floodFillData.finishX,floodFillData.finishY ,mapData.map[floodFillData.finishY][ floodFillData.finishX]));
    floodFillData.path.clear();
    //prehladavanie
    while(!openLayerList.empty()){
        //printf("Size: %d",openLayerList.size());
        Node *actualNode = openLayerList[0];
        openLayerList.erase(openLayerList.begin());
        //ak sa nasiel vysledny
        if(finishNode->getX() == actualNode->getX() &&finishNode->getY() == actualNode->getY() ){
            finishNode->setValue(actualNode->getValue());
            printf("NASIEL SOM %d",finishNode->getValue());

            actualNode = finishNode;
            //prehladavanie trasy a pridavanie jednotlivych bodov do pathu
            while(actualNode->getValue() != 2){
                bool added = false;

                printf("actual: %d, X: %d Y: %d \n",actualNode->getValue(),actualNode->getX(),actualNode->getY());
                if(mapData.map[actualNode->getY()+1][actualNode->getX()] == actualNode->getValue()-1 && !added){
                    floodFillData.path.push_back(new Node(actualNode->getX(),actualNode->getY()+1,mapData.map[actualNode->getY()+1][actualNode->getX()]));
                    actualNode = floodFillData.path.back();
                    added = true;
                }
                if(mapData.map[actualNode->getY()-1][actualNode->getX()]== actualNode->getValue()-1&& !added){
                    floodFillData.path.push_back(new Node(actualNode->getX(),actualNode->getY()-1,mapData.map[actualNode->getY()-1][actualNode->getX()]));
                    actualNode = floodFillData.path.back();
                    added = true;
                }
                if(mapData.map[actualNode->getY()][actualNode->getX()+1] == actualNode->getValue()-1&& !added){
                    floodFillData.path.push_back(new Node(actualNode->getX()+1,actualNode->getY(),mapData.map[actualNode->getY()][actualNode->getX()+1]));
                    actualNode = floodFillData.path.back();
                    added = true;
                }
                if(mapData.map[actualNode->getY()][actualNode->getX()-1] == actualNode->getValue()-1&& !added){
                    floodFillData.path.push_back(new Node(actualNode->getX()-1,actualNode->getY(),mapData.map[actualNode->getY()][actualNode->getX()-1]));
                    actualNode = floodFillData.path.back();
                }
            }

            floodFillData.path.push_back(startNode);

            //filter data detekcia rohov
            floodFillData.filteredPath.clear();
            floodFillData.filteredPath.push_back(floodFillData.path[0]);
            bool change = false;
            for(int i = 1; i<  floodFillData.path.size()-1; i++){
                if(!change &&(floodFillData.path[i]->getY() != floodFillData.path[i-1]->getY() && floodFillData.path[i]->getX() != floodFillData.path[i+1]->getX())){

                    floodFillData.filteredPath.push_back(floodFillData.path[i]);
                    change = true;
                }
                if(!change &&(floodFillData.path[i]->getY() != floodFillData.path[i+1]->getY() && floodFillData.path[i]->getX() != floodFillData.path[i-1]->getX())){

                    floodFillData.filteredPath.push_back(floodFillData.path[i]);
                    change = true;
                }
                if(change && (floodFillData.path[i]->getX() != floodFillData.path[i-1]->getX() && floodFillData.path[i]->getY() != floodFillData.path[i+1]->getY())){
                    floodFillData.filteredPath.push_back(floodFillData.path[i]);
                    change = false;

                }
                if(change && (floodFillData.path[i]->getX() != floodFillData.path[i+1]->getX() && floodFillData.path[i]->getY() != floodFillData.path[i-1]->getY())){
                    floodFillData.filteredPath.push_back(floodFillData.path[i]);
                    change = false;

                }

            }
            floodFillData.filteredPath.push_back(startNode);

            for(int i = 0; i< floodFillData.filteredPath.size(); i++){
                printf("Novy: %d, X: %d Y: %d \n",floodFillData.filteredPath[i]->getValue(),floodFillData.filteredPath[i]->getX(),floodFillData.filteredPath[i]->getY());
            }
            //mame filtrovane data enablujeme navigaciu
            floodFillData.enabled = true;
            floodFillData.filteredPath.erase(floodFillData.filteredPath.begin());
            navigationData.minDistance = 0.01;
            navigationData.deadAngle = degreeToRad(2);
            navigationData.p = 1000;
            navigationData.p_angle = 2;
            navigationData.max_speed = 400;
            break;

        }
        //nie sme vo finishe potrebujeme zistit susedov
        vector<Node*> neighbourList;

        if(mapData.map[actualNode->getY()+1][actualNode->getX()] == 0){
            neighbourList.push_back(new Node(actualNode->getX(),actualNode->getY()+1,mapData.map[actualNode->getY()+1][actualNode->getX()]));
        }
        if(mapData.map[actualNode->getY()-1][actualNode->getX()] == 0){
            neighbourList.push_back(new Node(actualNode->getX(),actualNode->getY()-1,mapData.map[actualNode->getY()-1][actualNode->getX()]));
        }
        if(mapData.map[actualNode->getY()][actualNode->getX()+1] == 0){
            neighbourList.push_back(new Node(actualNode->getX()+1,actualNode->getY(),mapData.map[actualNode->getY()][actualNode->getX()+1]));
        }
        if(mapData.map[actualNode->getY()][actualNode->getX()-1] == 0){
            neighbourList.push_back(new Node(actualNode->getX()-1,actualNode->getY(),mapData.map[actualNode->getY()][actualNode->getX()-1]));
        }

        //mame susedov oznacime
        for(Node *node : neighbourList){
            if(mapData.map[node->getY()][node->getX()] == 1 || std::find(closeLayerList.begin(), closeLayerList.end(), node) != closeLayerList.end()){
                printf("\n continue");
                continue;

            }else{
                mapData.map[node->getY()][node->getX()] = actualNode->getValue()+1;
                node->setValue(actualNode->getValue()+1);
                openLayerList.push_back(node);
                //printf("\n openclose");
                //closeLayerList.push_back(node);
            }
        }
    }

    //vypis
    for(int rows = 0; rows < 80; rows++)
    {
        for(int columns = 0; columns < 80; columns++)
        {
            if(mapData.map[rows][columns] == 1){
                printf("1,");
            }else{
                printf("%d,",mapData.map[rows][columns]);
            }

        }
        printf("\n");
    }
}

void MainWindow::navigateThroughGrid(){
    if(floodFillData.enabled && !navigationData.enabled ){
        double x = 0.15*(floodFillData.filteredPath[0]->getX()-39);
        double y = 0.15*(39- floodFillData.filteredPath[0]->getY());
        double distance = sqrt(pow(locationData.x - x, 2) + pow(locationData.y - y, 2));

        if(distance < navigationData.minDistance){
            if( floodFillData.filteredPath.size()>=2){
                floodFillData.filteredPath.erase(floodFillData.filteredPath.begin());
            }else{
                floodFillData.enabled = false;
                printf("FINISHED NAVIGATING");
            }
            std::vector<unsigned char> mess=robot.setTranslationSpeed(0);
            if (sendto(rob_s,  (char*)mess.data(),  sizeof(char)*mess.size(),  0,  (struct sockaddr*) &rob_si_posli,  rob_slen) == -1)
            {

            }
        }else{
            navigationData.errorAngle = locationData.angle - atan2((y-locationData.y),(x-locationData.x));
            navigationData.errorAngle = navigationData.errorAngle-2.0*M_PI*floor(navigationData.errorAngle/(2.0*M_PI)+0.5);

            if(-navigationData.deadAngle <= navigationData.errorAngle && navigationData.deadAngle >= navigationData.errorAngle ){

                float speed = 0;
                //pohyb
                if(distance*navigationData.p > navigationData.max_speed){
                    speed = navigationData.max_speed;
                }else{
                    speed = distance*navigationData.p;
                }

                std::vector<unsigned char> mess=robot.setTranslationSpeed(speed);
                if (sendto(rob_s,  (char*)mess.data(),  sizeof(char)*mess.size(),  0,  (struct sockaddr*) &rob_si_posli,  rob_slen) == -1) { }
                mapData.isRotating = false;
            }else{
                mapData.isRotating = true;
                if(-navigationData.deadAngle <= navigationData.errorAngle){
                    std::vector<unsigned char> mess = robot.setRotationSpeed(-abs(navigationData.errorAngle*navigationData.p_angle));
                    if (sendto(rob_s,  (char*)mess.data(),  sizeof(char)*mess.size(),  0,  (struct sockaddr*) &rob_si_posli,  rob_slen) == -1){}
                }else{
                    std::vector<unsigned char> mess = robot.setRotationSpeed(abs(navigationData.errorAngle*navigationData.p_angle));
                    if (sendto(rob_s,  (char*)mess.data(),  sizeof(char)*mess.size(),  0,  (struct sockaddr*) &rob_si_posli,  rob_slen) == -1){}
                }

            }

        }
    }
}


void MainWindow::on_pushButton_7_clicked()
{
    if (!bugData.enabled){
        bugData.finishX = ui->lineEdit_10->text().toDouble();
        bugData.finishY = ui->lineEdit_11->text().toDouble();
        bugData.enabled = true;
        bugData.zoneDiameter = 0.4;
        navigationData.minDistance = 0.05;
        navigationData.deadAngle = degreeToRad(2);
        navigationData.p = 1000;
        navigationData.p_angle = 2;
        navigationData.max_speed = 400;
        bugData.corners.clear();
    }
}

void MainWindow::findCorners(){
    if(bugData.enabled && !mapData.isRotating && !navigationData.enabled){
        double lastX;
        double lastY;
        double lastDistance=-1;
        double finishDistance = sqrt(pow(locationData.x - bugData.finishX , 2) + pow(locationData.y -bugData.finishY , 2));
        double finishAngle = asin((bugData.finishY-locationData.y)/finishDistance);
        bool firstCorner = true;
        bugData.finishSeen = true;

        for(int k=0;k<copyOfLaserData.numberOfScans/*360*/;k++){

            double dist = copyOfLaserData.Data[k].scanDistance/1000;//mm to m
            double angle = ((360.0-copyOfLaserData.Data[k].scanAngle)*3.14159/180.0);

            if(dist>0){
                double x = locationData.x+dist*cos(locationData.angle + angle);
                double y = locationData.y+dist*sin(locationData.angle + angle);

                if((lastDistance*0.9 <= dist && dist<=lastDistance*1.10) || (lastDistance == dist)){

                }else if(lastDistance == 0){
                    bugData.corners.push_back(new Corner(x, y, angle, false));
                }else if(lastDistance != -1){
                    if(lastDistance< dist){
                        bugData.corners.push_back(new Corner(lastX, lastY, angle, true));
                    }else{
                        bugData.corners.push_back(new Corner(lastX, lastY, angle, false));
                    }
                }

                if(((angle+locationData.angle) <= (locationData.angle-finishAngle +degreeToRad(90))) && ((angle+locationData.angle) >= (locationData.angle-finishAngle-degreeToRad(90)))){
                    double zoneDist  =  (bugData.zoneDiameter)/abs(sin(angle+locationData.angle-finishAngle));
                    double zoneMaxLength = sqrt(pow(finishDistance,2)+ pow(bugData.zoneDiameter,2));
                    if(zoneDist> zoneMaxLength){
                        zoneDist = zoneMaxLength;
                    }

                    if(dist!=0 && zoneDist>dist ){
                        bugData.finishSeen = false;
                    }
                }
                lastX = x;
                lastY = y;
            }
            lastDistance = dist;
        }
        if(!bugData.finishSeen){
            for(Corner *corner : bugData.corners){
                double cornerDistance = sqrt(pow(locationData.x - corner->getX(), 2) + pow(locationData.y - corner->getY(), 2));
                double newDistance = cornerDistance + sqrt(pow(corner->getX() - bugData.finishX , 2) + pow(corner->getY() - bugData.finishY , 2));
                double dCrit = sqrt(pow(cornerDistance, 2)+pow(bugData.zoneDiameter, 2));
                double angleCrit = asin(bugData.zoneDiameter/dCrit); // b/d angle od cornera
                double angle;
                dCrit += bugData.zoneDiameter;

                if(firstCorner){
                    firstCorner = false;
                    lastDistance = newDistance;
                }
                if(lastDistance >= newDistance){
                    lastDistance = newDistance;
                    //vypocet bodu kam sa ma dostat

                    bugData.nextX = corner->getX();
                    bugData.nextY = corner->getY();
                    if(corner->isOuterLeft()){
                        angle = corner->getAngle() + angleCrit;
                    }else{
                        angle = corner->getAngle() - angleCrit;
                    }

                    bugData.nextX = locationData.x+dCrit*cos(locationData.angle + angle);
                    bugData.nextY = locationData.y+dCrit*sin(locationData.angle + angle);
                    bugData.cornerFound = true;
                }
            }

            bugData.corners.clear();
        }
    }
}


void MainWindow::bugAlgorithm(){
    if(bugData.enabled && bugData.cornerFound && !navigationData.enabled){
        bugData.cornerFound = false;
        navigationData.x = bugData.nextX;
        navigationData.y = bugData.nextY;
        printf("X:%f Y: %f \n", bugData.nextX, bugData.nextY);
        navigationData.enabled = true;

    }else if(bugData.finishSeen){
        navigationData.x = bugData.finishX;
        navigationData.y = bugData.finishY;
        navigationData.enabled = true;
        bugData.enabled = false;
        bugData.finishSeen = false;
        bugData.cornerFound = false;

    }
}

